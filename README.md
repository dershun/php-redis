# util

#### 介绍
PHP Redi扩展封装

#### 安装

```
composer require dershun/php-redis
```
#### 使用说明

``` 
use Dershun\Util\RedisString 
$config = [
    'host'       => '127.0.0.1',
    'port'       => 6379,
    'password'   => '',
    'select'     => 0,
    'timeout'    => 0,
    'expire'     => 0,
    'persistent' => false,
    'prefix'     => ''
];
$redis = new RedisString($config);
$redis->set($name, $value, $expireIn);
$redis->get($name);
```

1. string类型

``` 
use Dershun\Util\RedisString
```
2. Stream类型

``` 
use Dershun\Util\RedisString
```