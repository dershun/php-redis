<?php
/*
 * @Author: juneChen && junechen_0606@163.com
 * @Date: 2023-06-26 16:20:56
 * @LastEditors: juneChen && junechen_0606@163.com
 * @LastEditTime: 2023-06-26 16:20:56
 * @Description: Redis
 * 
 */

declare(strict_types=1);

namespace Dershun\PhpRedis;

use DateInterval;
use DateTime;
use DateTimeInterface;

abstract class Redis
{
    /**
     * Redis 配置
     *
     * @var array
     */
    protected $config = [
        'host'       => '127.0.0.1',
        'port'       => 6379,
        'password'   => '',
        'select'     => 0,
        'timeout'    => 0,
        'expire'     => 0,
        'persistent' => false,
        'prefix'     => ''
    ];

    /**
     * Redis实例
     *
     * @var \Redis
     */
    protected $Redis;

    /**
     * 读取次数
     * @var integer
     */
    protected $readTimes = 0;

    /**
     * 写入次数
     * @var integer
     */
    protected $writeTimes = 0;

    /**
     * 构造函数
     */
    public function __construct(array $config = [])
    {
        if (!empty($config)) {
            $this->config = array_merge($this->config, $config);
        }
        if (extension_loaded('redis')) {
            $this->Redis = new \Redis;
            try {
                if ($this->config['persistent']) {
                    $osg = $this->Redis->pconnect($this->config['host'], $this->config['port'], $this->config['timeout'], 'persistent_id_' . $this->config['select']);
                } else {
                    $osg = $this->Redis->connect($this->config['host'], $this->config['port'], $this->config['timeout']);
                }
            } catch (\Exception $e) {
                throw new \BadFunctionCallException('connection failed: redis');
            }
            if (!$osg) {
                throw new \BadFunctionCallException('initialization failed: redis');
            }
            if (!empty($this->config['password'])) {
                $password = $this->Redis->auth($this->config['password']);
                if (!$password) {
                    throw new \BadFunctionCallException('password failed: redis');
                }
            }
            if ($this->config['select'] > 0) {
                $select = $this->Redis->select($this->config['select']);
                if (!$select) {
                    throw new \BadFunctionCallException('switch database: redis');
                }
            }
        } else {
            throw new \BadFunctionCallException('not support: redis');
        }
    }

    /**
     * 判断数据
     * @access public
     * @param string $name 数据变量名
     * @return bool
     */
    public function has(string $name): bool
    {
        return $this->Redis->exists($this->getCacheKey($name)) ? true : false;
    }

    /**
     * 删除数据
     * @access public
     * @param string $name 数据变量名
     * @return bool
     */
    public function delete(string $name): bool
    {
        $this->writeTimes++;

        $key    = $this->getCacheKey($name);
        $result = $this->Redis->del($key);
        return $result > 0;
    }

    /**
     * 清除数据
     * @access public
     * @return bool
     */
    public function clear(): bool
    {
        $this->Redis->flushDB();
        return true;
    }

    /**
     * 获取实际的数据标识
     * @access public
     * @param string $name 数据名
     * @return string
     */
    public function getCacheKey(string $name): string
    {
        return $this->config['prefix'] . $name;
    }

    /**
     * 获取有效期
     * @access protected
     * @param integer|DateTimeInterface|DateInterval $expire 有效期
     * @return int
     */
    protected function getExpireTime($expire): int
    {
        if ($expire instanceof DateTimeInterface) {
            $expire = $expire->getTimestamp() - time();
        } elseif ($expire instanceof DateInterval) {
            $expire = DateTime::createFromFormat('U', (string) time())
                ->add($expire)
                ->format('U') - time();
        }

        return (int) $expire;
    }

    /**
     * 获取配置项
     * @param  string $name 配置项名称
     * @return [type]       [description]
     */
    public function getConfig(string $name)
    {
        return $this->config[$name];
    }
}
