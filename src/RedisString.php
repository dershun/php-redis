<?php
/*
 * @Author: juneChen && junechen_0606@163.com
 * @Date: 2023-06-28 18:51:38
 * @LastEditors: juneChen && junechen_0606@163.com
 * @LastEditTime: 2023-06-28 18:51:38
 * @Description: Redis string类型
 * 
 */

declare(strict_types=1);

namespace Dershun\PhpRedis;

class RedisString extends Redis
{

    /**
     * 构造函数
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }


    /**
     * 读取数据
     * @access public
     * @param string $name    数据变量名
     * @param mixed  $default 默认值
     * @return mixed
     */
    public function get(string $name, $default = null)
    {
        $this->readTimes++;
        $key   = $this->getCacheKey($name);
        $value = $this->Redis->get($key);

        if (false === $value || is_null($value)) {
            return $default;
        }

        return unserialize($value);
    }

    /**
     * 写入数据
     * @access public
     * @param string            $name   数据变量名
     * @param mixed             $value  存储数据
     * @param integer|\DateTime $expire 有效时间（秒）
     * @return bool
     */
    public function set(string $name, $value, $expire = null): bool
    {
        $this->writeTimes++;

        if (is_null($expire)) {
            $expire = $this->config['expire'];
        }

        $key    = $this->getCacheKey($name);
        $expire = $this->getExpireTime($expire);
        $value  = serialize($value);

        if ($expire) {
            $result = $this->Redis->setex($key, $expire, $value);
        } else {
            $result = $this->Redis->set($key, $value);
        }

        return $result;
    }

    /**
     * 自增数据（针对数值数据）
     * @access public
     * @param string $name 数据变量名
     * @param int    $step 步长
     * @return false|int
     */
    public function inc(string $name, int $step = 1)
    {
        $this->writeTimes++;
        $key = $this->getCacheKey($name);

        return $this->Redis->incrby($key, $step);
    }

    /**
     * 自减数据（针对数值数据）
     * @access public
     * @param string $name 数据变量名
     * @param int    $step 步长
     * @return false|int
     */
    public function dec(string $name, int $step = 1)
    {
        $this->writeTimes++;
        $key = $this->getCacheKey($name);

        return $this->Redis->decrby($key, $step);
    }
}
