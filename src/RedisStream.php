<?php
/*
 * @Author: juneChen && junechen_0606@163.com
 * @Date: 2023-06-28 19:42:04
 * @LastEditors: juneChen && junechen_0606@163.com
 * @LastEditTime: 2023-06-28 19:42:04
 * @Description: Redis Stream类型
 * 
 */

declare(strict_types=1);

namespace Dershun\PhpRedis;

class RedisStream extends Redis
{

    /**
     * 构造函数
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }


    /**
     * 读取数据
     * @access public
     * @param string $name    数据变量名
     * @param mixed  $default 默认值
     * @return array
     */
    public function get(string $name, $default = []): array
    {
        $this->readTimes++;
        $key   = $this->getCacheKey($name);
        $value = $this->Redis->xRead([$key => '0-0']);

        if (false === $value || is_null($value)) {
            return $default;
        }

        return $value[$key];
    }

    /**
     * 读取第一条数据
     *
     * @param string $name   数据变量名
     * @param array $default 默认值
     * @return array
     * @author juneChen <junechen_0606@163.com>
     */
    public function first(string $name, $default = []): array
    {
        $this->readTimes++;
        $key   = $this->getCacheKey($name);
        $value = $this->Redis->xRead([$key => '0-0'], 1);

        if (false === $value || is_null($value)) {
            return $default;
        }

        return $value[$key];
    }

    /**
     * 写入数据
     * @access public
     * @param string            $name   数据变量名
     * @param array             $value  存储数据
     * @param string            $id     条目ID
     * @param integer           $leng   长度
     * @return string|false
     */
    public function set(string $name, array $value, string $id = '*', int $leng = 0)
    {
        $this->writeTimes++;
        $key = $this->getCacheKey($name);
        $result = $this->Redis->xAdd($key, $id, $value, $leng, true); # 设置数据流的最大长度
        return $result;
    }

    /**
     * 删除数据
     * @access public
     * @param string $name 数据变量名
     * @param array  $ids  要删除的ID数组
     * @return bool
     */
    public function del(string $name, array $ids): bool
    {
        $this->writeTimes++;

        $key    = $this->getCacheKey($name);
        $result = $this->Redis->xDel($key, $ids);
        return $result > 0;
    }
}
